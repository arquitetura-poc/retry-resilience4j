## Problema
Recentimente precisei realizar uma integração com uma api financeira. A operação era simples, eu precisava realizar um requisição para um webservice e ele me retornaria uma lista de informações sobre uma determinada ação. Segui todos os passos recomendados pela documentação da api:

- Realizar as chamadas de sincronização apenas uma vez por dia;
- Evitar de realizar inúmeras requisições simultâneas;
- Não passar filtros de datas com grandes intervalos, etc;

A feature foi implementada e homologada no ambiente de testes da api. Tudo funcionou perfeitamente bem e sentimos que já era hora de colocar a feature produção. Passados apenas três dias, a funcionalidade começou a apresentar um comportamento estranho. A medida que o número de clientes crescia, as requisições aumentavam, e apesar de seguir todas as recomendações, api ora funcionada ora não. 

É como dizem, se é pra dá errado dará assim que colocar em produção.

![murphy](../assets/murphy.jpg)

## Solução 1: Exponential Backoff

Como o tempo estava apertado e precisariamos manter a feature em produção, implementamos um algoritmo de recuo exponencial. 

Esta estratégia consiste em aplicar um intervalo cada vez maior, ate um limite fixo, em caso de falha. Geralmente ela é utilizada para evitar congestão de rede, sobrecargas ou consumo desnecessário de recursos. Caso haja falhas seguidas em determinado intervalo, não adiantará insistir da mesma forma.

O algoritmo abaixo é bem fiel a solução implementada:


```java

public static final int MAX_RETRIES = 5;

public static foo() {
 for (int i = 0; i < MAX_RETRIES; i++) {
  try {
   // RESQUEST PARA A API FINANCEIRA.
   return;
  } catch (ClientException e) {
   if (e.getErrCode().contains("Rejected.Throttling")) {//need retry
    try {
     Thread.sleep(getWaitTimeExponential(i + 1));
    } catch (InterruptedException ignore) {
    }
   }
  }
 }
}

public static long getWaitTimeExponential(int retryCount) {
 final long initialDelay = 200L;
 long waitTime = ((long) Math.pow(2, retryCount) * initialDelay);
 return waitTime;
}

```

No final, a solução resolveu magicamente o problema enfrentado anteriormente.

![murphy](../assets/magic.gif)

## Solução 2: Retry pattern

Apesar da solução anterior funcionar, ainda não estava satisfeito. Após algumas pesquisas me deparei com a biblioteca **Ressilience4j**. Ela é uma biblioteca leve que implementa várias soluções de tolerância a falhas. A sua simplicidade e facilidade de uso foi um dos principais motivos que me levou a adota-la. Ela prove os seguintes padrões de resiliêcia:

- Circuit Breaker
- RateLimiter
- TimeLimiter
- Retry
- Bulkhead
- Cache

### Padrão Retry:

Cada padrão resolve um problema específico. Existem alguns cenários o padrão retry pode ser empregado, por exemplo:

- Serviços temporariamente indisponíveis;
- Perda de conexão;
- Timeout em algumas requisições;
- Ou seja, erros que acontecem em interva-los aleatórios.

Através deste padrão, um consumidor é capaz de realizar novamente uma ação, sempre que  uma situação imprevisível ocorra. Lembre-se, este padrão deve ser utilizado apenas para erros que aconteçam de forma esporádica e que sejam capaz de se "autoresolver".

#### Código de exemplo

De acordo com o a [documentação oficial](https://resilience4j.readme.io/docs/getting-started) , devemos utilizar java 8 ou superior. Se você utiliza maven ou gradle, basta incluir o módulo de acordo com os snippets abaixo.

```xml
<dependency>
	<groupId>io.github.resilience4j</groupId>
	<artifactId>resilience4j-spring-boot2</artifactId>
	<version>1.7.0</version>
</dependency>
```

```java

repositories {
    mavenCentral()
}

dependencies {
  implementation "io.github.resilience4j:resilience4j-spring-boot2:1.7.0"
}
```

Após a importação das depêndencias, devemos configurar algumas propriedades para o bom funcionamento da lib. No aplication.yml insira o trecho abaixo.

```yaml
resilience4j:
  retry:
    instances:
      unstable-service:
        max-attempts: 5
        wait-duration: 1s
        enable-exponential-backoff: true
        exponential-backoff-multiplier: 2
        retry-exceptions:
          - org.springframework.web.client.HttpClientErrorException
```

Na configuração anterior, informamos para lib que ela deverá tentar realizar ação por 5 vezes, caso a exception ```HttpClientErrorException``` seja disparada. Além disto, habilitamos o  reuco exponêncial.

Com o projeto devidamente configurado, chegou a hora de implementarmos a nova solução. Como dito anteriormente, a biblioteca é bastante simples de se utilizar. Devemos apenas anotar o método que queremos que o *retry* ocorra.

```java
@Slf4j
@Service
public class ConsumerService {

	private static final String URL = "https://dummy.restapiexample.com/api/v1/employees";

	@Autowired
	RestTemplate restTemplate;
	
	@Retry(name = "unstable-service", fallbackMethod = "fallbackAfterRetry")
	public String fooWithRetry() {
		return restTemplate.getForObject(URL + "/1", String.class);
	}
	
	public String fallbackAfterRetry(Exception ex) {
		log.error("Servico instavel, exception: ", ex);
		return "timeout";
	}

}
```

Viu como é simples? Basta anotar a tarefa, informando a configuração *(unstable-service)* registrada no ```application.yml```. Além disto, devemos informar um método para tratar o fluxo, caso o problema não se resolva depois das 5 tentativas.

![easy](../assets/easy.gif)

package br.com.retry.retrypoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetryPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetryPocApplication.class, args);
	}

}

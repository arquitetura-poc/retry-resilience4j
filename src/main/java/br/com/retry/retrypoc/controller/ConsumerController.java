package br.com.retry.retrypoc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.retry.retrypoc.service.ConsumerService;

@RestController
public class ConsumerController {

	@Autowired
	ConsumerService consumerService;

	@GetMapping("/instavel")
	public String unstable() {
		return consumerService.unstable();
	}

	@GetMapping("/instavel-com-retry")
	public String unstableWithRetry() {
		return consumerService.unstableWithRetry();
	}
}

package br.com.retry.retrypoc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ConsumerService {

	private static final String URL = "https://dummy.restapiexample.com/api/v1/employees";

	@Autowired
	RestTemplate restTemplate;

	public String unstable() {
		return restTemplate.getForObject(URL + "/1", String.class);
	}

	@Retry(name = "unstable-service", fallbackMethod = "fallbackAfterRetry")
	public String unstableWithRetry() {
		return restTemplate.getForObject(URL + "/1", String.class);
	}
	
	public String fallbackAfterRetry(Exception ex) {
		log.error("Servico instavel, exception: ", ex);
		return "timeout";
	}

}

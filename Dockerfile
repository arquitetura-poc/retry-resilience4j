FROM maven:3.8.3-openjdk-17 AS build
WORKDIR /app 
COPY . .
RUN mvn clean install -DskipTests

FROM openjdk:17-alpine3.14
EXPOSE 8080
COPY --from=build /app/targ/*.jar /poc.jar
ENTRYPOINT ["java", "-jar", "/poc.jar"]
